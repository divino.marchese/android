package com.example.genji.am404_handlerthread;

import android.os.Handler;
import android.os.HandlerThread;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Handler handler = new Handler(); // to communicate with UI Thread
    private MyHandlerThread myHandlerThread;

    Button btnStart;
    ProgressBar progressBar;
    TextView textMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = findViewById(R.id.progress);
        textMsg = findViewById(R.id.msg);
        btnStart = findViewById(R.id.start);

        myHandlerThread = new MyHandlerThread("myHandlerThread");

        final Runnable myRunnable = () -> {
                for (int i = 0; i <= 10; i++) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    //is accessed from within inner class, needs to be declared final
                    final int finalI = i;
                    handler.post(() -> progressBar.setProgress(finalI));
                }
                handler.post(() -> textMsg.setText("finished"));
        };

        myHandlerThread.start();
        myHandlerThread.prepareHandler();

        btnStart.setOnClickListener((View v) ->
                myHandlerThread.postTask(myRunnable)
        );
    }

    @Override
    protected void onDestroy() {
        myHandlerThread.quit();
        super.onDestroy();
    }


    public class MyHandlerThread extends HandlerThread {

        private Handler handler;

        public MyHandlerThread(String name) {
            super(name);
        }

        public void postTask(Runnable task){
            handler.post(task);
        }

        public void prepareHandler(){
            handler = new Handler(getLooper());
        }
    }
}
