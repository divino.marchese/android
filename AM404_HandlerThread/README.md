# AM404_HandlerThread

Questo è in primo esempio dell'utilizzo di un HandlerThread cui viene sotto posto un unico task, quindi l'esercitazione ha solo un valore esemplificativo, [qui](https://developer.android.com/reference/android/os/HandlerThread) le API.
Su **Medium** un'interessante lettura: [qui](https://medium.com/@ali.muzaffar/handlerthreads-and-why-you-should-be-using-them-in-your-android-apps-dc8bf1540341).