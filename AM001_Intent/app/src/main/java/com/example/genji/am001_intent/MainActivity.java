package com.example.genji.am001_intent;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* set listener old fashion
        Button btn = (Button) findViewById(R.id.ba);
        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, ActivityTwo.class);
                startActivity(i);
            }
        });
        */
        Button btn1 = findViewById(R.id.activity);
        btn1.setOnClickListener((View view) -> {
            Intent i = new Intent(this, Activity2.class);
            startActivity(i);
        });

    }

    public void onClick(View view) {
        Intent i = new Intent(Intent.ACTION_VIEW); // try to remove action
        switch(view.getId()){
            case R.id.map:
                String url_position = "geo:45.495403,12.257509";
                i.setData(Uri.parse(url_position));
                break;
            case R.id.browser:
                String url = "http://www.itiszuccante.gov.it";
                i.setData(Uri.parse(url));
        }
        startActivity(i);
    }
}
