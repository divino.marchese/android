package com.example.genji.am014_listfragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment; // deprecated fragments from ....
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class MyListFragment extends ListFragment implements AdapterView.OnItemClickListener {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // create the adapter
        ArrayAdapter adapter = ArrayAdapter.createFromResource(getActivity(), R.array.products, android.R.layout.test_list_item);
        // connect the adapter
        setListAdapter(adapter);
        // connect the listener
        getListView().setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

        String[] myResArray = getResources().getStringArray(R.array.products);
        List<String> productsListList = Arrays.asList(myResArray);
        Toast.makeText(getActivity(), "Item: " + productsListList.get(position) + " at position " +
                position, Toast.LENGTH_SHORT).show();
        
    }
}
