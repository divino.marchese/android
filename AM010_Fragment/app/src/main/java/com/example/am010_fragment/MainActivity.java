package com.example.am010_fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Create a new Fragment to be placed in the activity layout
        FragmentOne fr = new FragmentOne();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.add(R.id.fragment_container, fr);
        fragmentTransaction.commit();
    }

    public void selectFrag(View view) {
        FragmentManager fm = getSupportFragmentManager();
        Fragment fr;
        // if(view == findViewById(R.id.btn2) && findViewById(R.id.f1) != null){
        if(view == findViewById(R.id.btn2) && fm.findFragmentById(R.id.fragment_container) instanceof FragmentOne){
            fr = new FragmentTwo();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fr);
            fragmentTransaction.commit();
        //} else if (view == findViewById(R.id.btn1) && findViewById(R.id.f2) != null){
        } else if (view == findViewById(R.id.btn1) && fm.findFragmentById(R.id.fragment_container) instanceof FragmentTwo){
            fr = new FragmentOne();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fr);
            fragmentTransaction.commit();
        }
    }
}
