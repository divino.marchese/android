# AM015_ViewHolder

Il pattern del **ViewHolder** ha ancora la sua valenza concettuale! Dalla documentazione; da leggere assolutamente la documentazione di `View` riguardo ai [Tag]/https://developer.android.com/reference/android/view/View) ed in particolare il metodo `public void setTag (Object tag)` - anche il suo simile ... - ed il suo simmetrico `public Object getTag ()`. 

Android Developer propone la cosa [qui](https://developer.android.com/training/improving-layouts/smooth-scrolling). Inoltre nella documentazione troviamo

## getView

added in API level 1
```
public View getView (int position, 
                View convertView, 
                ViewGroup parent)
```
Get a View that displays the data at the specified position in the data set. You can either create a View manually or inflate it from an XML layout file. When the View is inflated, the parent View (GridView, ListView...) will apply default layout parameters unless you use LayoutInflater.inflate(int, android.view.ViewGroup, boolean) to specify a root view and to prevent attachment to the root.

Abbiamo quindi il compito o di creare manualmente la `View` o di chiamare il *service* `LayoutInflater` per tradurre `XML`. Per migliorare le prestazioni, se ciò è possibile, possiamo usare il parametro `convertView` per riciclare view già create senza usare ad esempio `findViewById` per recuperare un `XML` di volta in volta riadattandolo ai nostri scopi (alla riga che dobbiamo creare).

