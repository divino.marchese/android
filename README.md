# Android Mini Course

Ecco la sequenza delle nostre esercitazioni.


## Starting here

- AM301: sintassi lambda

## Activity & Intent

- AM001_Intent: Intent implicito ed eseplicito.
- AM002_Intent: ancora sulla gestione delle Activity e l'uso degli Intent.
- AM006_Activity: ciclo di vita, Log e notifiche.
- AM302_splash: un'activity senza layout.
- AM005_Map: Intent implicito con App standard e personali (AM007_MyBrowser)

## Fragment & Dialogs

- AM010_Fragment: primo esercizio sui Fragment.
- AM012_Fragment: secondo esercizio sui Fragment con animazioni.
- AM008_AlertDialog: FragmentDialog per Picker e Dialog.
- AM009_Fragments: **pattern** comunicazione fra due fragment di una stessa activity.

## ListView and RecyclerView

- AM013_ListView.
- AM014_ListFragment.
- AM015_ViewHolder: **pattern** del ViewHolder.
- AM306_RecyclerView

## Broadcast and System Services

- AM003_Receiver
- AM003_Receiver: qui viene introdotto il `PendingIntent`
- AM305_DownloadManager: esempio articolato di approfondimento

## Threading

- AM401_Threading: un **working thread** comunica con **UI-thread**.
- AM402_Handler: entriamo nel dettaglio del threading.
- AM403_Handler_Message: un esempio di messaggio.
- AM027_AsyncTask
- AM404_HandlerThread
- AM405_HandlerThread_Message

## Retrofit 2

- AM104_GSON: in questo primo esempio scarichiamo tramite **GSON** un unico oggetto json da un file in rete.
- AM101_Mongo: in questo primo esempio ci connettiamo al server web **REST** [RestHeart](http://restheart.org/quick-start.html).
- AM309_Retrofit2_Upload: upload di immagine usando [okhttp3](http://square.github.io/okhttp/) con server **node**.

## Esercitazioni

- PP001_FlatDice: un gioco con i Fragment e non solo.
