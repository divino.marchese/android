## AM006_ActivityLifeCycle

Questa è un'applicazione pensata per vedere come e quando operano i metodi che abbiamo visto nel ciclo di vita delle Activity.

Per i **log**: [qui](https://developer.android.com/studio/debug/am-logcat). Useremo spesso il test 
```
if (BuildConfig.DEBUG) {
            ...
        }
```
tale test risulta vero almeno che generiamo (vedi Build) non generiamo il `Signed APK`. Qui lo presentiamo per mostrare come gestire i LOG, utilissimi per un debug iniziale (da usare senza tema). A tal proposito abbimo definito un nostro TAG:
```
ublic interface MyLOG {
    String TAG = "MyLOG";
}
```
Per la gestione delle notifiche rimandiamo alla documentazione [qui](https://developer.android.com/training/notify-user/build-notification).