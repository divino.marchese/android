package com.example.am006_activitylifecycle;


import android.os.Bundle;

public class SecondActivity extends TracingActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
    }
}
