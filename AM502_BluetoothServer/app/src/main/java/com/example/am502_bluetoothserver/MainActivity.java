package com.example.am502_bluetoothserver;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {

    @SuppressLint("HandlerLeak")
    private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            String string = bundle.getString("msg");
            Log.w("BTTAG", "text: " + string);
            Snackbar.make(findViewById(android.R.id.content), string, Snackbar.LENGTH_LONG)
                    .show();
        }
    };




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BluetoothTools.enable(this);

        final BluetoothTools.ServerThread server = new BluetoothTools.ServerThread(handler);

        Button start = findViewById(R.id.start);
        Button stop = findViewById(R.id.stop);

        start.setOnClickListener((View view) -> {
            server.start();
        });
        stop.setOnClickListener((View view) -> {
            server.cancel();
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
