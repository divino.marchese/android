package com.example.am502_bluetoothserver;

public interface MessageConstants {
    int MESSAGE_READ = 0;
    int MESSAGE_WRITE = 1;
    int MESSAGE_CONNECTED = 2;
    int GENERIC_ERROR = 4;
    int MESSAGE_GENERIC = 5;
}
