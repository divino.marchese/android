package com.example.am502_bluetoothserver;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.android.material.snackbar.Snackbar;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.UUID;

public class BluetoothTools {

    private static final int REQUEST_ENABLE_BT = 666;
    private static final UUID MY_UUID = UUID.fromString("38400000-8cf0-11bd-b23e-10b96e4ef00d");


    private static BluetoothAdapter bluetoothAdapter;

    // enable BT and obtain adapter
    static void enable(Activity activity){
        // try to get bluetoothAdapter
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            // android.R.id.content is the root layout of activity
            Snackbar.make(activity.findViewById(android.R.id.content), "BT is not available", Snackbar.LENGTH_LONG)
                    .show();
            Log.i("TAGBT", "BT is not available ");

        }
        if (!bluetoothAdapter.isEnabled()) {
            Snackbar.make(activity.findViewById(android.R.id.content), "enable BT", Snackbar.LENGTH_LONG)
                    .show();
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            Log.i("TAGBT", "BT is enabled");
        }
    }

    static class ServerThread extends Thread {

        private Handler handler;
        private boolean isRunning = false;

        private final BluetoothServerSocket serverSocket;
        InputStream inStream;
        OutputStream outStream;

        ServerThread(Handler handler) {
            super();
            this.handler = handler;
            // try to initialize the socket
            BluetoothServerSocket tmp = null;
            try {
                // MY_UUID is the app's UUID string, also used by the client code.
                tmp = bluetoothAdapter.listenUsingRfcommWithServiceRecord("server", MY_UUID);
            } catch (IOException e) {
                Log.e("BTTAG", "Socket's listen() method failed", e);
                Bundle bundle = new Bundle();
                bundle.putString("msg", "server thread not created");
                Message errorMsg = handler.obtainMessage(
                        MessageConstants.GENERIC_ERROR);
                errorMsg.setData(bundle);
                handler.sendMessage(errorMsg);
            }
            serverSocket = tmp;
            Log.w("BTTAG", "Created");
            Bundle bundle = new Bundle();
            bundle.putString("msg", "server thread created");
            Message genericMsg = handler.obtainMessage(
                    MessageConstants.MESSAGE_GENERIC);
            genericMsg.setData(bundle);
            handler.sendMessage(genericMsg);
        }


        public void run() {
            Log.w("BTTAG", "start running");
            Bundle bundle = new Bundle();
            bundle.putString("msg", "start running");
            Message genericMsg = handler.obtainMessage(
                    MessageConstants.MESSAGE_GENERIC);
            genericMsg.setData(bundle);
            genericMsg.sendToTarget();
            BluetoothSocket socket;
            // Keep listening until exception occurs or a socket is returned.
            isRunning = true;
            while (isRunning) {
                try {
                    socket = serverSocket.accept();
                } catch (IOException e) {
                    Log.e("BTTAG", "Socket's accept() method failed", e);
                    break;
                }
                if (socket != null) {
                    // A connection was accepted. Perform work associated with
                    // the connection in a separate thread.
                    manageMyConnectedSocket(socket);
                    cancel();
                } else {
                    Log.e("BTTAG", "Socket is null");
                }

            }
        }

        private void manageMyConnectedSocket(BluetoothSocket socket) {

            Log.w("BTTAG", "create and manage connection");

            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams; using temp objects because
            // member streams are final.
            try {
                tmpIn = socket.getInputStream();
            } catch (IOException e) {
                Log.e("BTTAG", "Error occurred when creating input stream", e);
            }
            try {
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e("BTTAG", "Error occurred when creating output stream", e);
            }

            inStream = tmpIn;
            outStream = tmpOut;

            try {
                PrintWriter out = new PrintWriter(outStream, true);
                BufferedReader in = new BufferedReader(new InputStreamReader(inStream));
                String inputLine, outputLine;
                while((inputLine = in.readLine()) != null){
                    Log.e("BTTAG", "inputLine: " + inputLine);
                    outputLine = inputLine;
                    out.println("received: " + outputLine);
                }
            } catch(Exception e) {
                e.printStackTrace();
            }
        }

        // closes the connect socket  stop running
        void cancel() {
            if(!isRunning)
                return;
            isRunning = false;
            Bundle bundle = new Bundle();
            bundle.putString("msg", "server stopped");
            Message genericMsg = handler.obtainMessage(
                    MessageConstants.MESSAGE_GENERIC);
            genericMsg.setData(bundle);
            genericMsg.sendToTarget();
            try {
                serverSocket.close();
            } catch (IOException e) {
                Log.e("BTTAG", "Could not close the connect socket", e);
            }
        }
    }
}
