package com.example.genji.am016_sqlite;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.List;


@Entity(tableName = "products")
public class Product {

    @PrimaryKey(autoGenerate = true)
    int id;

    @ColumnInfo(name = "name")
    String name;

    @ColumnInfo(name = "description")
    String description;

    public Product(@NonNull String name, @NonNull String description){
        this.name = name;
        this.description = description;
    }

    // this filed will be ignored

    @Ignore
    static Product[] products = {
            new Product("gioppini", "panetti sfiziosi"),
            new Product("jambonetti", "salatini al prosciutto"),
            new Product("patatine sfizione", "patatine salate ed aromatizzate"),
            new Product("tarallini", "anelli di pane"),
            new Product("gallette", "gallette plus"),
            new Product("frollini plus", "biscotti all'uovo"),
            new Product("cioccolini", "frollini con gocce di cioccolata"),
            new Product("secchini", "biscotti secchi"),
            new Product("grissinini", "grissini piccoli e sottili"),
            new Product("patasplash", "le patatine da bordo piscina"),
            new Product("majopatas", "le patatine aromatizzate .."),
            new Product("crocchette al sesamo", "panetti al sesamo"),
            new Product("crocchette alla pancetta", "panetti alla pancetta"),
            new Product("biscotti al miglio e avena", "i biscotti cinguettanti")
    };

}
