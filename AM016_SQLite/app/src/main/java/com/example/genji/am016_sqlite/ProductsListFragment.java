package com.example.genji.am016_sqlite;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProductsListFragment extends ListFragment implements AdapterView.OnItemClickListener {

    AppDatabase db;
    List<Product> products;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        db = AppDatabase.getAppDatabase(context.getApplicationContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // products from model (testing)
        // products = new ArrayList<>(Arrays.asList(Product.products));
        products = getAll();

        ProductsArrayAdapter adapter = new ProductsArrayAdapter(getActivity(), (ArrayList<Product>) products);
        //use this below for a correct initialization
        setListAdapter(adapter);

        // connect to
        getListView().setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(getActivity(), "Item: " + position + "\n name: " + (products.get(position)).name + "\n" +
                "description: " + (products.get(position)).description, Toast.LENGTH_SHORT).show();
    }

    List<Product> getAll(){
        return db.productDao().getAll();
    }
}
