package com.example.genji.am016_sqlite;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    AppDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = AppDatabase.getAppDatabase(getApplicationContext());
    }

    public void startOperate(View view){
        Intent i = new Intent(this, OperateActivity.class);
        startActivity(i);
    }

    public void init(View view){
        populateWithTestData();
        // DB transaction
        /*
        pds.open();
        pds.initialize();
        pds.close();
        */
    }

    public void clearDB(View view){
        deleteAll();
        // DB transaction
        /*
        pds.open();
        pds.deleteAll();
        pds.close();
        */
    }

    Product addUser(Product product) {
        db.productDao().insertAll(product);
        return product;
    }

    void populateWithTestData() {
        db.productDao().insertAll(Product.products);
    }

    void deleteAll(){
        db.productDao().deleteAll();
    }

}
