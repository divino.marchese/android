package com.example.genji.am016_sqlite;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.ArrayList;
import java.util.List;

// Data Access Objects

@Dao
public interface ProductDao {

    @Query("SELECT * FROM products")
    List<Product> getAll();

    @Query("SELECT * FROM products WHERE id IN (:productIds)")
    List<Product> loadAllByIds(int[] productIds);

    @Query("SELECT * FROM products WHERE name LIKE :name AND " +
            "description LIKE :description LIMIT 1")
    Product findByName(String name, String description);

    @Insert
    void insertAll(Product... products);

    @Delete
    void delete(Product user);

    @Query("DELETE FROM products")
    void deleteAll();

}
