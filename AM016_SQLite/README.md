# AM016_SQLite

Questa è la nuova versione dell'esercitazione su SQLite che fa uso di **Room Persistence Library**: [qui](https://developer.android.com/training/data-storage/room/).

## Room

Nel diagramma che segue è illustrato il meccanismo di connessione.
![schema](https://developer.android.com/images/training/data-storage/room_architecture.png)


## Entity/Model

Nella classe model usiamo **annotation**, per queste rimandiamo alle [lezioni](https://docs.oracle.com/javase/tutorial/java/annotations/) del tutorial Oracle, si guardi inoltre il tutorial di Jenkov: [qui](http://tutorials.jenkov.com/java-reflection/annotations.html). Ecco un esempio
```
@Entity(tablename = "users")
public class User {

   @PrimaryKey(autogenerate = true)
   private final int uid;

   private String name;

   @ColumnInfo(name = "last_name")
   private String lastName;

   public User(name, lastName) {
       ...
   }
   ...
 }
 ```
Tale esempio è abbastanzo autoesplicativo, resta solo da dire che `@PrimaryKey(autogenerate = true)` permette a SQLite di dare in modo autoincrementato il valore alla chiave un po' come tipicamente accade, `@Entity(tablename = "users")` permette di legare l'entità alla tabella il cui nome tipicamente è plurale.

### DAO

Data Access Object ci permette di operare sul DB
```
@Dao
 public interface UserDao {
   @Query("SELECT * FROM user")
   List<User> loadAll();

   @Query("SELECT * FROM user WHERE uid IN (:userIds)")
   List<User> loadAllByUserId(int... userIds);

   @Query("SELECT * FROM user where name LIKE :first AND last_name LIKE :last LIMIT 1")
   User loadOneByNameAndLastName(String first, String last);

   @Insert
   void insertAll(User... users);

   @Delete
   void delete(User user);

   @Query("DELETE FROM user")
   void deleteAll();
 }
 ```

### AppDatabase

Ecco qui sotto il codice (quello della nostra esercitazione) **singleton** è il design pattern utilizzato
```
@Database(entities = {Product.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public abstract ProductDao productDao();

    static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context,
                    AppDatabase.class, "products")
                    .allowMainThreadQueries()
                    .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
```
Per cambiare la versione utilizziamo le `DatabaseMigration` di cui qui non trattiamo: [qui](https://developer.android.com/training/data-storage/room/migrating-db-versions). Per legare al suo DAO il db usiamo il metodo ` public abstract ProductDao productDao()`. Invitiamo anche alla lettura della documentazione della classe `RoomDatabase`: [qui](https://developer.android.com/reference/android/arch/persistence/room/RoomDatabase).

### Heleper

Una volta si procedeva creando una classe tipo `MySQLiteHelper` che abbiamo mantenuto nell'esempio senza usarla. 

## Altri Materiali

Qui sotto alcune proposte di lettura.  
[1] Un articolo di Anitaa Murthy [5 steps to implement Room ...](https://android.jlelse.eu/5-steps-to-implement-room-persistence-library-in-android-47b10cd47b24).  