# AM003_BroadcastReceiver

In questo esercizio vediamo il `BoradcastReceiver`, [qui](https://developer.android.com/guide/components/broadcasts.html) per una guida, ed alcuni `Service` di sistema. Per le API: [qui](https://developer.android.com/reference/android/content/BroadcastReceiver.html). Nel manifest abbiamo messo
```
<receiver android:name=".MyBroadcastReceiver"  android:exported="true">
    <intent-filter>
        <action android:name="android.intent.action.BOOT_COMPLETED"/>
        <action android:name="android.intent.action.INPUT_METHOD_CHANGED" />
   </intent-filter>
</receiver>
```
prendendo dalla documentazione, era sufficiente solo la registrazione!

## AlarmManager

Il nostro primo service di sistema, per un uso vedi [qui](https://developer.android.com/training/scheduling/alarms), useremo: `RTC_WAKEUP`  
> Wakes up the device and fires the pending intent after the specified length of time has elapsed since device boot.

## Publish-Subscribe pattern

E' il modello di riferimento per questa particolare gestione dei messaggi; una sua descrizione tratta da Wikipedia (en): [qui](https://en.wikipedia.org/wiki/Publish%E2%80%93subscribe_pattern).

## Manifest-declared receivers

E' la prima tipologia, esemplificata in questo esempio ([qui](https://developer.android.com/guide/components/broadcasts.html)). Nel nostro caso chiameremo il broadcast addirittura in modo esplicito con un Intent di siffatta specie.

## Context-registered receivers

In caso si voglia procedere con una maggiore flessibilità si procede in modo programmatico ([qui](https://developer.android.com/guide/components/broadcasts.html)).

Nel nostro esempio il `BoradcastReceiver` si trova all'interno della stessa app, comunque la comunicazione avviene in questo senso
```
i) creazione dell'intent esplicito ---> 
ii) l'Intent viene consegnato al service di allarme come PendingIntent ---> 
iii) Android con il suo servizio di allarme rilancia l'intent ed il nostro Broadcast (e non altri) lo riceve.
```
## Wrapping di un Intent

Viene utilizzato un `PendingIntent`; per la API: [qui](https://developer.android.com/reference/android/app/PendingIntent.html).
