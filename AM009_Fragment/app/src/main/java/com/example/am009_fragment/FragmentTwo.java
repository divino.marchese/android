package com.example.am009_fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FragmentTwo extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_two,
                container, false);
        return view;
    }

    public void setText(String txt) {
        // getView() : Get the root view for the fragment's layout (fragment must have a layout)
        TextView view = getView().findViewById(R.id.detailsText);
        view.setText(txt);
    }
}
