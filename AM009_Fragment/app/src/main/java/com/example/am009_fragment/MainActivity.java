package com.example.am009_fragment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements FragmentOne.MyListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onItemSelected(String txt) {
        FragmentTwo fragment = (FragmentTwo) getSupportFragmentManager()
                .findFragmentById(R.id.fragmentTwo);
        fragment.setText(txt);
    }
}
