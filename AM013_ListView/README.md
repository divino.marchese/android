# AM_013_ListView

L'interesse di questo codice sta nell'uso del **design pattern** dell'adapter. Da tener conto il metodo 
```
public void setOnItemClickListener (AdapterView.OnItemClickListener listener)
```
di `AdaperView`, per ripasso andare qui su [generic](https://docs.oracle.com/javase/tutorial/java/generics/unboundedWildcards.html).  In
```
onItemClick(AdapterView<?> parent, View view, int position, long id) 
```
`position` e `id` nel caso di una `ListAdaper` coincidono come valore. Attenti all'uso di
```
adapter.notifyDataSetChanged();
```