package com.example.am007_intentfilter;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView textView = findViewById(R.id.textView);
        Intent i = getIntent();
        String data = "No ghe xe";
        Uri uri = i.getData();
        if (uri != null) {
            data = uri.toString();
        }
        textView.setText(data);

    }
}
