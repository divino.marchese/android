# AM007_IntentFilter

Nella documentazione invitiamo a leggere [qui](https://developer.android.com/training/basics/intents/filters.html) e [qui](https://developer.android.com/guide/components/intents-filters).
Qui creiamo un'app è molto semplice. Notiamo quanto segue.
```
<uses-permission android:name="android.permission.INTERNET" />
```
sul manifest, in realtà a noi non serve per ora ma sta ad indicare la possibilità che questa App ha di andare in Internet come indicato [qui](https://developer.android.com/reference/android/Manifest.permission.html#INTERNET).

Questa app possiede una sola Activity significativa e tale Activity **deve** essere lanciata con un intent implicito
```
<intent-filter>
    <action android:name="android.intent.action.VIEW" />
    <category android:name="android.intent.category.DEFAULT" />
    <category android:name="android.intent.category.BROWSABLE"
    <data android:scheme="http" />
</intent-filter>
```
L'azione è standard
```
<action android:name="android.intent.action.VIEW"
```
è obbligatorio
```
 <category android:name="android.intent.category.DEFAULT"
```
inoltre
```
 <category android:name="android.intent.category.BROWSABLE"
```
in quanto  
> The target activity allows itself to be started by a web browser to display data referenced by a link, such as an image or an e-mail message. 

per poter avviare da altrove; filtriamo gli URI
```
<data android:scheme="http"
```
a tal proposito rimandiamo alla documentazione: [qui](https://developer.android.com/guide/topics/manifest/data-element.html).

Per terminare facciamo osservare che potevamo lavorare solo con la MainActivity con due **intent filter**.

Un ultimo dettaglio:
```
tools:ignore="AppLinkUrlError"
```
è ad uso dell'IDE.


