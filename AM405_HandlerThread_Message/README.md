# AM405_HandlerThread_Message

Ad un HandlerThread vengono, in questo caso, mabndati dei **messaggi** i quali, a loro volta, sono gestiti usando il metodo
```
@Override
        protected void onLooperPrepared() {
            super.onLooperPrepared();
            handler = new Handler(getLooper()) {

                @Override
                public void handleMessage(Message msg) {
                    switch (msg.what) {
                        case WHAT1:
                            doSomething(msg);
                            break;
                        case WHAT2:
                            doSomething(msg);
                            break;
                    }
                }
            };
        }
```
differenziando in funzione di `what`. L'esempio è *insulso* ma serve a dare uno schema per la gestione coi messaggi per compiti un po' raffinati. Usando un singolo handler all'interno della inner class si realizza una successione *sincrona* dei task associati ai messaggi (i.e. bloccante). 