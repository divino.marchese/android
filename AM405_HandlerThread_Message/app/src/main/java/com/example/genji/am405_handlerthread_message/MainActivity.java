package com.example.genji.am405_handlerthread_message;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.Process;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import static com.example.genji.am405_handlerthread_message.MainActivity.MyHandlerThread.WHAT1;
import static com.example.genji.am405_handlerthread_message.MainActivity.MyHandlerThread.WHAT2;

public class MainActivity extends AppCompatActivity {

    private MyHandlerThread myHandlerThread;
    private Handler uiHandler = new Handler();

    Button btnWhat1;
    Button btnWhat2;
    TextView textMsg1;
    TextView textMsg2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnWhat1 = findViewById(R.id.what1);
        btnWhat2 = findViewById(R.id.what2);
        textMsg1 = findViewById(R.id.msg1);
        textMsg2 = findViewById(R.id.msg2);

        btnWhat1.setOnClickListener((View v) -> {
            Message msg = myHandlerThread.getMessage();
            msg.what = WHAT1;
            myHandlerThread.sendMessage(msg);
        });

        btnWhat2.setOnClickListener((View v) -> {
            Message msg = myHandlerThread.getMessage();
            msg.what = WHAT2;
            myHandlerThread.sendMessage(msg);
        });

        myHandlerThread = new MyHandlerThread();
        myHandlerThread.start();

    }

    @Override
    protected void onDestroy() {
        myHandlerThread.quit();
        super.onDestroy();
    }

    public void doSomething(Message msg){
        for (int i = 0; i <= 10; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            final String counter = String.valueOf(i);
            switch (msg.what) {
                case WHAT1:
                    uiHandler.post(() -> textMsg1.setText(counter));
                    break;
                case WHAT2:
                    uiHandler.post(() -> textMsg2.setText(counter));
                    break;
            }
        }
        String text = "FINISH: " + msg.what;
        switch (msg.what) {
            case WHAT1:
                uiHandler.post(() -> textMsg1.setText(text));
                break;
            case WHAT2:
                uiHandler.post(() -> textMsg2.setText(text));
                break;
        }
    }



    public class MyHandlerThread extends HandlerThread {

        static final int WHAT1 = 1;
        static final int WHAT2 = 2;

        private Handler handler;

        MyHandlerThread() {
            // THREAD_PRIORITY_BACKGROUND = 10, is standard priority
            super("MyHandlerThread", Process.THREAD_PRIORITY_BACKGROUND);
        }

        // Call back method that can be explicitly overridden
        // if needed to execute some setup before Looper loops

        @Override
        protected void onLooperPrepared() {
            super.onLooperPrepared();
            handler = new Handler(getLooper()) {

                @Override
                public void handleMessage(Message msg) {
                    switch (msg.what) {
                        case WHAT1:
                            doSomething(msg);
                            break;
                        case WHAT2:
                            doSomething(msg);
                            break;
                    }
                }
            };
        }

        public Message getMessage(){
            return handler.obtainMessage();
        }

        public void sendMessage(Message msg){
            handler.sendMessage(msg);
        }
    }
}
