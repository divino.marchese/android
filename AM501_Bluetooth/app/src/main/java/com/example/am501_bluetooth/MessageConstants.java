package com.example.am501_bluetooth;

public interface MessageConstants {
    int MESSAGE_READ = 0;
    int MESSAGE_WRITE = 1;
    int MESSAGE_CONNECTED = 2;
    int GENERIC_ERROR = 4;
}
