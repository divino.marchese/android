package com.example.am501_bluetooth;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

public class BTActivity extends AppCompatActivity {

    BluetoothDevice device;
    TextView read;

    @SuppressLint("HandlerLeak")
    private final Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch(msg.what){
                case MessageConstants.MESSAGE_READ: {
                    String readMsg = msg.getData().getString("msg");
                    Log.w("BRRAG", "read: " + readMsg);
                    read.setText(readMsg);
                    break;
                }
                case MessageConstants.MESSAGE_WRITE: {
                    String writeMsg = msg.getData().getString("msg");
                    Snackbar.make(findViewById(android.R.id.content), "sending:" + writeMsg, Snackbar.LENGTH_LONG)
                            .show();
                    break;
                }
                case MessageConstants.GENERIC_ERROR: {
                    String errorMsg = msg.getData().getString("msg");
                    Snackbar.make(findViewById(android.R.id.content), errorMsg, Snackbar.LENGTH_LONG)
                            .show();
                    break;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bt);
        // get device
        device = getIntent().getExtras().getParcelable("BTDEVICE");

        // start the bluetooth thread connecting the server
        BluetoothTools.ConnectionThread thread = new BluetoothTools.ConnectionThread(device, handler);
        thread.start();

        TextView name = findViewById(R.id.btName);
        name.setText(device.getName());
        EditText sendMsg = findViewById(R.id.btWrite);
        Button btnSend = findViewById(R.id.btSend);
        read = findViewById(R.id.btRead);

        btnSend.setOnClickListener((View view) -> {
            String msg = sendMsg.getText().toString();
            thread.write(msg);
        });
    }
}
