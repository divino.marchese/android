package com.example.am501_bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.android.material.snackbar.Snackbar;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Set;
import java.util.UUID;


public class BluetoothTools {

    private static final int REQUEST_ENABLE_BT = 666;
    private static final UUID MY_UUID = UUID.fromString("38400000-8cf0-11bd-b23e-10b96e4ef00d");

    private static BluetoothAdapter bluetoothAdapter;

    // enable BT and obtain adapter
    static void enable(Activity activity){
        // try to get bluetoothAdapter
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            // android.R.id.content is the root layout of activity
            Snackbar.make(activity.findViewById(android.R.id.content), "BT is not available", Snackbar.LENGTH_LONG)
                    .show();
            Log.i("TAGBT", "BT is not available ");

        }
        if (!bluetoothAdapter.isEnabled()) {
            Snackbar.make(activity.findViewById(android.R.id.content), "enable BT", Snackbar.LENGTH_LONG)
                    .show();
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            Log.i("TAGBT", "BT is enabled");

        }
    }

    static void discoverDevice(Activity activity){
        if(bluetoothAdapter.isDiscovering()) {
            bluetoothAdapter.cancelDiscovery();
        }
        Snackbar.make(activity.findViewById(android.R.id.content), "discover unpaired devices", Snackbar.LENGTH_LONG)
                .show();
        bluetoothAdapter.startDiscovery();
        Log.i("TAGBT", "BT: start discovering devices");

    }

    static void stopDiscover(){
        bluetoothAdapter.cancelDiscovery();
        Log.i("TAGBT", "BT: stop discovering devices");

    }

    static boolean testPairedDevice(BluetoothDevice device){

        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0 ) {
            for (BluetoothDevice dev : pairedDevices) {
                if(device == dev) return true;
            }
        }
        return false;
    }

    static class ConnectionThread extends Thread {

        private BluetoothDevice device;
        private Handler handler;

        BluetoothSocket socket;
        InputStream inStream;
        OutputStream outStream;

        ConnectionThread(BluetoothDevice device, Handler handler) {
            super();
            this.device = device;
            this.handler = handler;
        }


        public void run() {

            // Cancel discovery because it otherwise slows down the connection.
            bluetoothAdapter.cancelDiscovery();

            // avoid null device
            if (device == null ) {
                Log.i("TAGBT", "device is NULL");
                Bundle bundle = new Bundle();
                bundle.putString("msg","device is NULL");
                Message errorMsg = handler.obtainMessage(
                        MessageConstants.GENERIC_ERROR);
                errorMsg.setData(bundle);
                errorMsg.sendToTarget();
                return;
            }

            // create the socket
            BluetoothSocket tmp = null;
            try {
                // Get a BluetoothSocket to connect with the given BluetoothDevice.
                // MY_UUID is the app's UUID string, also used in the server code.
                tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
                Log.e("TAGBT", "Socket's create() method failed", e);
                Bundle bundle = new Bundle();
                bundle.putString("msg","unable to create socket");
                Message errorMsg = handler.obtainMessage(
                        MessageConstants.GENERIC_ERROR);
                errorMsg.setData(bundle);
                errorMsg.sendToTarget();
                return;
            }
            socket = tmp;

            try {
                // Connect to the remote device through the socket. This call blocks
                // until it succeeds or throws an exception.
                socket.connect();
            } catch (IOException connectException) {
                Log.e("TAGBT", "unable to connect to server");
                Bundle bundle = new Bundle();
                bundle.putString("msg","unable to connect to the socket");
                Message errorMsg = handler.obtainMessage(MessageConstants.GENERIC_ERROR);
                errorMsg.setData(bundle);
                errorMsg.sendToTarget();
                try {
                    socket.close();
                } catch (IOException closeException) {
                    Log.e("TAGBT", "Could not close the client socket", closeException);
                }
            }

            // Get the input and output streams; using temp objects because
            // member streams are final.
            InputStream tmpIn = null;
            OutputStream tmpOut = null;
            try {
                tmpIn = socket.getInputStream();
            } catch (IOException e) {
                Log.e("BTTAG", "Error occurred when creating input stream", e);
            }
            try {
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e("BRTAG", "Error occurred when creating output stream", e);
            }
            inStream = tmpIn;
            outStream = tmpOut;

            BufferedReader reader = new BufferedReader(new InputStreamReader(inStream));

            // Keep listening to the InputStream until an exception occurs.
            while (true) {
                try {
                    // read only one line
                    String msg = reader.readLine();
                    Bundle bundle = new Bundle();
                    bundle.putString("msg", msg);
                    Message readMsg = handler.obtainMessage(MessageConstants.MESSAGE_READ);
                    readMsg.setData(bundle);
                    readMsg.sendToTarget();
                } catch (IOException e) {
                    Log.d("BTTAG", "Input stream was disconnected", e);
                    break;
                }
            }

        }

        // Call this from the main activity to send data to the remote device.
        // in asynchronous way
        void write(String msg) {

            PrintWriter outWriter = new PrintWriter(outStream, true);

            try {
                outWriter.println(msg);
                // outWriter.flush();
                Bundle bundle = new Bundle();
                bundle.putString("msg", msg);
                Message writtenMsg = handler.obtainMessage(MessageConstants.MESSAGE_WRITE);
                writtenMsg.setData(bundle);
                writtenMsg.sendToTarget();
            } catch (Exception e) {
                Log.e("BTTAG", "Error occurred when sending data", e);
                Bundle bundle = new Bundle();
                bundle.putString("msg","Error occurred when sending data");
                Message errorMsg = handler.obtainMessage(MessageConstants.GENERIC_ERROR);
                errorMsg.setData(bundle);
                errorMsg.sendToTarget();



            }
        }


        // Closes the client socket and causes the thread to finish.
        public void cancel() {
            try {
                socket.close();
            } catch (IOException e) {
                Log.e("TAGBT", "Could not close the client socket", e);
            }
        }
    }
}
