package com.example.am501_bluetooth;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements MyRecyclerViewAdapter.ItemClickListener {

    MyRecyclerViewAdapter adapter;
    ArrayList<BluetoothDevice> devices;

    private BroadcastReceiver scannerReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // recyclerview
        // data to populate the RecyclerView with
        devices = new ArrayList<>();

        // set up the RecyclerView
        RecyclerView recyclerView = findViewById(R.id.btDevices);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MyRecyclerViewAdapter(this, devices);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);

        // enable BT if it is possible
        BluetoothTools.enable(this);

        // register for broadcasts when a device is discovered.
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        scannerReceiver = getScannerReceiver();
        registerReceiver(scannerReceiver, filter);

        // floating button
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener((View view) -> {
            devices.clear();
            adapter.notifyDataSetChanged();
            BluetoothTools.discoverDevice(this);
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Don't forget to unregister the ACTION_FOUND receiver.
        unregisterReceiver(scannerReceiver);
    }


    // menu management

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    // a BroadcastReceiver factory method for scanning nearby unpaired devices
    BroadcastReceiver getScannerReceiver() {
        return new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    // Discovery has found a device. Get the BluetoothDevice
                    // object and its info from the Intent.
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                    if(!BluetoothTools.testPairedDevice(device)){
                        MainActivity.this.devices.add(device);
                        MainActivity.this.adapter.notifyDataSetChanged();
                    }


                    String deviceName = device.getName();
                    String deviceHardwareAddress = device.getAddress(); // MAC address
                    Log.i("TAGBT", "Device found: " + deviceName + "; MAC " + deviceHardwareAddress);
                    // BluetoothTools.stopDiscover();
                }
            }
        };
    }


    // implementing recycler view
    @Override
    public void onItemClick(View view, int position) {
        BluetoothDevice device = adapter.getItem(position);
        Toast.makeText(this, "You clicked " + device + " on row number " + position, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, BTActivity.class);
        // add a parceable
        intent.putExtra("BTDEVICE", device);
        startActivity(intent);
    }



}
